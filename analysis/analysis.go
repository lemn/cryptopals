package analysis

import (
	"sort"
	"strconv"
	"unicode"

	"cryptopals/crypto"
)

type KeyDistance struct {
	Size int
	Dist float32
}

type KeyDistances []KeyDistance

func (x KeyDistances) Len() int {
	return len(x)
}

func (x KeyDistances) Less(i, j int) bool {
	return x[i].Dist < x[j].Dist
}

func (x KeyDistances) Swap(i, j int) {
	x[i], x[j] = x[j], x[i]
}

func ScoreString(a string) int {
	var i int
	for _, r := range a {
		if unicode.IsUpper(r) {
			i++
		} else if unicode.IsLower(r) {
			i += 2
		} else if unicode.IsSpace(r) {
			i += 3
		}
	}
	return i
}

func CheckKey(a string, b string) crypto.XorResult {
	var result crypto.XorResult
	result.Key = b
	result.Value = crypto.DecodeXor(a, b)
	result.Score = ScoreString(result.Value)
	return result
}

func CheckAllKeys(a string) crypto.XorResults {
	var results []crypto.XorResult
	var i int64
	for i = 0x00; i <= 0xFF; i++ {
		results = append(results, CheckKey(a, strconv.FormatInt(i, 16)))
	}

	sort.Sort(crypto.XorResults(results))

	return results
}

// HammingDist returns the Hamming distance between two strings
func HammingDist(a, b []byte) int {
	if len(a) != len(b) {
		panic("Error: blocks must be of the same size to compute Hamming distance")
	}

	res := 0
	for i, v := range a {
		for j := 0; j < 8; j++ {
			mask := byte(1 << uint(j))
			if (v & mask) != (b[i] & mask) {
				res++
			}
		}
	}

	return res
}

// checkKeysize returns the average normalized Hamming distance for
// blocks of Keysize m evaluated against test string a
func CheckKeysize(a []byte, m int) KeyDistance {
	var dist float32
	blocksA := Partition(a, m)
	for i := 0; i < len(blocksA)-2; i++ {
		hammDist := HammingDist(blocksA[i], blocksA[i+1])
		hammDistNorm := float32(hammDist) / float32(m)
		dist = dist + hammDistNorm
	}
	res := KeyDistance{Size: m, Dist: dist / float32(len(blocksA))}
	return res
}

// checkAllKeysizes loops checkKeysize from 2 up to n byte key sizes
func CheckAllKeysizes(a []byte, n int) KeyDistance {
	var keydistances []KeyDistance
	for i := 2; i <= n; i++ {
		distance := CheckKeysize(a, i)
		keydistances = append(keydistances, distance)
	}
	sort.Sort(KeyDistances(keydistances))
	return keydistances[0]
}

// partition splits a ciphertext (or any []byte) into
// slice of []byte each of max length m
func Partition(a []byte, m int) [][]byte {
	var partition [][]byte
	for i := 0; i <= len(a)/m; i++ {
		end := (i + 1) * m
		if end > len(a) {
			end = len(a)
		}
		block := a[(i)*m : end]
		partition = append(partition, block)
	}
	return partition
}

// transpose a matrix expressed as byte slices
func Transpose(A [][]byte) [][]byte {
	var T [][]byte
	max := 0
	for _, v := range A {
		if len(v) >= max {
			max = len(v)
		}
	}
	for i := 0; i < max; i++ {
		T = append(T, []byte{})
	}
	for _, v := range A {
		for j, w := range v {
			T[j] = append(T[j], w)
		}
	}
	return T
}

func ScoreDuplicates(input string, length int) int {
	inBytes := []byte(input)
	counts := make(map[string]int)
	parts := Partition(inBytes, length)

	// count up duplicate byte partitions
	for _, p := range parts {
		_, e := counts[string(p)]
		if e {
			counts[string(p)]++
		} else {
			counts[string(p)] = 1
		}
	}

	// sum total occurrences for a total score
	score := 0
	for _, c := range counts {
		if c != 1 {
			score += c
		}

	}

	return score
}
