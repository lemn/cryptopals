package test

import (
	"cryptopals/crypto"
	"encoding/hex"
	"testing"
)

func TestS1P2a(t *testing.T) {

	expected := "746865206b696420646f6e277420706c6179"
	in1 := "1c0111001f010100061a024b53535009181c"
	in2 := "686974207468652062756c6c277320657965"
	actual := crypto.FixedXor(in1, in2)
	if actual != expected {
		t.Error("Challenge 2 failed")
	} else {
		t.Logf("Challenge 2 success: Value (%v)\n", actual)
	}

}

func TestS1P2b(t *testing.T) {

	expected := "746865206b696420646f6e277420706c6179"
	in1, _ := hex.DecodeString("1c0111001f010100061a024b53535009181c")
	in2, _ := hex.DecodeString("686974207468652062756c6c277320657965")

	//actual := fixedXor(in1, in2)
	actual := crypto.Xor(in1, in2)
	if hex.EncodeToString(actual) != expected {
		t.Error("Challenge 2 failed")
	} else {
		t.Logf("Challenge 2 success: Value (%v)\n", actual)
	}
}
