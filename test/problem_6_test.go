package test

import (
	"cryptopals/analysis"
	"cryptopals/crypto"
	"encoding/base64"
	"encoding/hex"
	"io/ioutil"
	"log"
	"testing"
)

func TestS1P6a(t *testing.T) {

	expected := 37
	in1 := "this is a test"
	in2 := "wokka wokka!!!"
	actual := analysis.HammingDist([]byte(in1), []byte(in2))

	if actual != expected {
		t.Errorf("Challenge 6a failed\n Value expected: \n(%v)\n Value actual \n(%v)", expected, actual)
	} else {
		t.Logf("Challenge 6a success: Value (%v)\n", actual)
	}
}

func TestS1P6b(t *testing.T) {

	expectedSize := 29

	data, err := ioutil.ReadFile("../data/6.txt")
	if err != nil {
		log.Fatal(err)
	}
	var binInput []byte
	binInput, _ = base64.StdEncoding.DecodeString(string(data))

	actual := analysis.CheckAllKeysizes(binInput, 40)

	if actual.Size != expectedSize {
		t.Errorf("Challenge 6b failed\n Size expected: \n(%v)\n Size actual \n(%v)", expectedSize, actual.Size)
	} else {
		t.Logf("Challenge 6b success: Size (%v)\n", actual.Size)
	}
}

func TestS1P6c(t *testing.T) {
	data, err := ioutil.ReadFile("../data/6c.txt")
	if err != nil {
		log.Fatal(err)
	}

	pActual := analysis.Partition(data, 4)
	tActual := analysis.Transpose(pActual)

	pExpected := [][]byte{{73, 99, 101, 32, 105, 115, 32}, {98, 97, 99, 107, 32, 119, 105}, {116, 104, 32, 97, 32, 98, 114}, {97, 110, 100, 32, 110, 101, 119}, {32, 105, 110, 118, 101, 110, 116}}
	tExpected := [][]byte{{73, 98, 116, 97, 32}, {99, 97, 104, 110, 105}, {101, 99, 32, 100, 110}, {32, 107, 97, 32, 118}, {105, 32, 32, 110, 101}, {115, 119, 98, 101, 110}, {32, 105, 114, 119, 116}}

	if pActual[0][0] != pExpected[0][0] {
		t.Errorf("Challenge 6c partition failed\n Value expected: \n(%v)\n Value actual \n(%v)", pExpected, pActual)
	} else {
		t.Logf("Challenge 6c partition success \n")
	}
	if tActual[0][0] != tExpected[0][0] {
		t.Errorf("Challenge 6c transpose failed\n Value expected: \n(%v)\n Value actual \n(%v)", tExpected, tActual)
	} else {
		t.Logf("Challenge 6c transpose success \n")
	}
}

func TestS1P6d(t *testing.T) {

	expectedKey := "5465726d696e61746f7220583a204272696e6720746865206e6f697365"

	data, err := ioutil.ReadFile("../data/6.txt")
	if err != nil {
		log.Fatal(err)
	}
	var binInput []byte
	binInput, _ = base64.StdEncoding.DecodeString(string(data))
	binInputT := analysis.Transpose(analysis.Partition(binInput, 29))

	var keys []crypto.XorResult
	for _, v := range binInputT {
		keys = append(keys, analysis.CheckAllKeys(hex.EncodeToString(v))[0])
	}
	var key string
	for _, v := range keys {
		key = key + v.Key
	}
	bKey, _ := hex.DecodeString(key)
	solution := crypto.RotatingXor(binInput, bKey)
	solutionStr := string(solution)

	if key != expectedKey {
		t.Errorf("Challenge 6d failed\nKey expected: \n(%v)\n Key actual \n(%v)", expectedKey, key)
	} else {
		t.Logf("Challenge 6d success:\nKey:\n(%v)\nSolution:\n(%v)[...]", key, solutionStr[:32])
	}
}
