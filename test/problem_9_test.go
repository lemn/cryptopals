package test

import (
	"cryptopals/encoding"
	"testing"
)

func TestS2P9(t *testing.T) {
	expected := "YELLOW SUBMARINE\x04\x04\x04\x04"
	actual := encoding.Pkcs7Pad([]byte("YELLOW SUBMARINE"), 20)

	if string(actual) != expected {
		t.Errorf("Challenge 9 failed\nValue expected: \n(%v)\n Value actual \n(%v)", []byte(expected), []byte(actual))
	} else {
		t.Logf("Challenge 9 success:\nValue:\n(%v)", actual)
	}
}
