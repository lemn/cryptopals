package test

import (
	"cryptopals/crypto"
	"encoding/base64"
	"io/ioutil"
	"log"
	"testing"
)

func TestS1P7(t *testing.T) {

	expected := "I'm back and I'm ringin' the bell"

	data, err := ioutil.ReadFile("../data/7.txt")
	if err != nil {
		log.Fatal(err)
	}

	in, _ := base64.StdEncoding.DecodeString(string(data))

	ecbCipher, err := crypto.NewECBCipher(16, []byte("YELLOW SUBMARINE"))
	if err != nil {
		log.Fatal(err)
	}

	actual := make([]byte, len(in))
	ecbCipher.Decrypt(actual, in)

	actualSub := string([]rune(string(actual))[0:33])

	if actualSub != expected {
		t.Errorf("Challenge 7 failed\nValue expected: \n(%v)\n Value actual \n(%v)", string(expected), actualSub)
	} else {
		t.Logf("Challenge 7 success:\nValue:\n(%v)[...]", string(actual)[:32])
	}
}
