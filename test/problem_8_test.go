package test

import (
	"bufio"
	"cryptopals/analysis"
	"log"
	"os"
	"sort"
	"testing"
)

func TestS1P8(t *testing.T) {

	expected := 133

	file, err := os.Open("../data/8.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	type lineScore struct {
		LineNum int
		Score   int
	}

	var results []lineScore
	line := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		score := analysis.ScoreDuplicates(scanner.Text(), 4)
		results = append(results, lineScore{line, score})
		line++
	}
	sort.Slice(results, func(i, j int) bool {
		return results[i].Score > results[j].Score
	})
	actual := results[0].LineNum

	if actual != expected {
		t.Errorf("Challenge 8 failed\nValue expected: \n(Line %v)\n Value actual \n(Line %v)", expected, actual)
	} else {
		t.Logf("Challenge 8 success:\nValue:\n(Line %v)", actual)
	}
}
