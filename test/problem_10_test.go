package test

import (
	"cryptopals/crypto"
	"cryptopals/encoding"
	"encoding/base64"
	"io/ioutil"
	"log"
	"testing"
)

func TestS2P10a(t *testing.T) {
	expected := "I'm back and I'm ringin' the bell"
	input := encoding.Pkcs7Pad([]byte(expected), 16)

	ecbCipher, err := crypto.NewECBCipher(16, []byte("YELLOW SUBMARINE"))
	if err != nil {
		log.Fatal(err)
	}

	cipherText := make([]byte, len(input))
	ecbCipher.Encrypt(cipherText, input)

	actual := make([]byte, len(cipherText))
	ecbCipher.Decrypt(actual, cipherText)
	actual, _ = encoding.Pkcs7Unpad(actual, 16)

	if string(actual) != expected {
		t.Errorf("Challenge 10a failed\nValue expected: \n(%v)\n Value actual \n(%v)", []byte(expected), actual)
	} else {
		t.Logf("Challenge 10a success:\nValue:\n(%v)", actual)
	}
}

func TestS2P10b(t *testing.T) {
	expected := "I'm back and I'm ringin' the bell"
	input := encoding.Pkcs7Pad([]byte(expected), 16)

	cbcCipher, err := crypto.NewCBCCipher(16, []byte("YELLOW SUBMARINE"), []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
	if err != nil {
		log.Fatal(err)
	}

	cipherText := make([]byte, len(input))
	cbcCipher.Encrypt(cipherText, input)

	actual := make([]byte, len(cipherText))
	cbcCipher.Decrypt(actual, cipherText)
	actual, _ = encoding.Pkcs7Unpad(actual, 16)

	if string(actual) != expected {
		t.Errorf("Challenge 10b failed\nValue expected: \n(%v)\n Value actual \n(%v)", expected, actual)
	} else {
		t.Logf("Challenge 10b success:\nValue:\n(%v)", actual)
	}
}

func TestS2P10c(t *testing.T) {
	expected, err := ioutil.ReadFile("../data/10-decrypted.txt")
	if err != nil {
		log.Fatal(err)
	}
	input, err := ioutil.ReadFile("../data/10.txt")
	if err != nil {
		log.Fatal(err)
	}

	cbcCipher, err := crypto.NewCBCCipher(16, []byte("YELLOW SUBMARINE"), []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
	if err != nil {
		log.Fatal(err)
	}

	cipherText := make([]byte, base64.StdEncoding.DecodedLen(len(input)))
	l, _ := base64.StdEncoding.Decode(cipherText, input)
	cipherText = cipherText[:l]

	actual := make([]byte, len(cipherText))
	cbcCipher.Decrypt(actual, cipherText)
	actual, _ = encoding.Pkcs7Unpad(actual, 16)

	if string(actual) != string(expected) {
		t.Errorf("Challenge 10c failed\nValue expected: \n(%v)\n Value actual \n(%v)", expected, string(actual))
	} else {
		t.Logf("Challenge 10c success:\nValue:\n(%v)[...]", string(actual)[0:33])
	}
}
