package test

import (
	"bufio"
	"cryptopals/analysis"
	"cryptopals/crypto"
	"log"
	"os"
	"sort"
	"testing"
)

func TestS1P4(t *testing.T) {

	expected := crypto.XorResult{Score: 65, Key: "35", Value: "Now that the party is jumping\n"}

	file, err := os.Open("../data/4.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var actual []crypto.XorResult
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		res := analysis.CheckAllKeys(scanner.Text())
		actual = append(actual, res...)
	}
	sort.Sort(crypto.XorResults(actual))

	// Our solution is the first ranked result
	if actual[0].Score != expected.Score {
		t.Errorf("Challenge 4 failed\n Score Expected: (%v)\n Score Actual: (%v)", expected.Score, actual[0].Score)
	} else if actual[0].Key != expected.Key {
		t.Errorf("Challenge 4 failed\n Key Expected: (%v)\n Key Actual: (%v)", expected.Key, actual[0].Key)
	} else if actual[0].Value != expected.Value {
		t.Errorf("Challenge 4 failed\n Value Expected: (%v)\n Value Actual: (%v)", expected.Value, actual[0].Value)
	} else {
		t.Logf("Challenge 4 success: Score (%v), Key (%v), Value (%v)", actual[0].Score, actual[0].Key, actual[0].Value)
	}

}
