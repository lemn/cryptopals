package test

import (
	"cryptopals/encoding"
	"testing"
)

func TestS1P1(t *testing.T) {

	expected := "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
	in := "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	actual := encoding.HexToBase64(in)
	if actual != expected {
		t.Error("Challenge 1 failed")
	} else {
		t.Logf("Challenge 1 success: Value (%v)\n", actual)
	}

}
