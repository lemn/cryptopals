package test

import (
	"cryptopals/crypto"
	"encoding/hex"
	"testing"
)

func TestS1P5(t *testing.T) {

	expected := "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
	in1 := "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
	in2 := "ICE"

	actual := crypto.RotatingXor([]byte(in1), []byte(in2))
	actualString := hex.EncodeToString(actual)

	if actualString != expected {
		t.Errorf("Challenge 5 failed\n Value expected: \n(%v)\n Value actual \n(%v)", expected, actual)
	} else {
		t.Logf("Challenge 5 success: Value (%v)\n", actual)
	}
}
