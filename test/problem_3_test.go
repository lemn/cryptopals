package test

import (
	"cryptopals/analysis"
	"cryptopals/crypto"
	"testing"
)

func TestS1P3(t *testing.T) {

	expected := crypto.XorResult{Score: 69, Key: "58", Value: "Cooking MC's like a pound of bacon"}
	in1 := "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
	actual := analysis.CheckAllKeys(in1)

	// Our solution is the first ranked result
	if actual[0].Score != expected.Score {
		t.Errorf("Challenge 3 failed\n Score Expected: (%v)\n Score Actual: (%v)", expected.Score, actual[0].Score)
	} else if actual[0].Key != expected.Key {
		t.Errorf("Challenge 3 failed\n Key Expected: (%v)\n Key Actual: (%v)", expected.Key, actual[0].Key)
	} else if actual[0].Value != expected.Value {
		t.Errorf("Challenge 3 failed\n Value Expected: (%v)\n Value Actual: (%v)", expected.Value, actual[0].Value)
	} else {
		t.Logf("Challenge 3 success: Score (%v), Key (%v), Value (%v)", actual[0].Score, actual[0].Key, actual[0].Value)
	}
}
