package encoding

import "errors"

func Pkcs7Pad(input []byte, bs int) []byte {
	padded := input
	padSize := bs - len(padded)%bs
	char := byte(padSize)
	for pad := padSize; pad > 0; pad-- {
		padded = append(padded, char)
	}
	return padded
}

func Pkcs7Unpad(input []byte, bs int) ([]byte, error) {
	char := input[len(input)-1]
	if char == 0 || char > byte(bs) {
		// not padded
		return nil, errors.New("pkcs7: invalid format")
	}

	padding := input[len(input)-int(char):]
	for _, p := range padding {
		if p != char {
			// not actually padded
			return nil, errors.New("pkcs7: invalid format")
		}
	}
	// do unpadding
	return input[:len(input)-int(char)], nil
}
