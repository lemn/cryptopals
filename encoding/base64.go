package encoding

import (
	"encoding/base64"
	"encoding/hex"
)

func HexToBase64(a string) string {
	hexBytes, _ := hex.DecodeString(a)
	b64String := base64.StdEncoding.EncodeToString(hexBytes)
	return b64String
}
