package crypto

import "encoding/hex"

type XorResult struct {
	Value string
	Key   string
	Score int
}

type XorResults []XorResult

func (x XorResults) Len() int {
	return len(x)
}

func (x XorResults) Less(i, j int) bool {
	return x[i].Score > x[j].Score
}

func (x XorResults) Swap(i, j int) {
	x[i], x[j] = x[j], x[i]
}

// RotatingXor implements a rotating xor cypher using input a and key b
func RotatingXor(a []byte, b []byte) []byte {
	var out []byte
	for i, v := range a {
		x := v ^ b[i%len(b)]
		out = append(out, x)
	}
	return out
}

func Xor(a, b []byte) (out []byte) {
	for i, v := range a {
		out = append(out, v^b[i])
	}
	return
}

func DecodeXor(a string, b string) string {
	var bLong string
	for range a {
		bLong += b
	}
	out, _ := hex.DecodeString(FixedXor(a, bLong))
	return string(out)
}

func FixedXor(a string, b string) string {
	var out []byte
	a2, _ := hex.DecodeString(a)
	b2, _ := hex.DecodeString(b)
	for i, v := range a2 {
		x := v ^ b2[i]
		out = append(out, x)
	}
	res := hex.EncodeToString(out)
	return res
}
