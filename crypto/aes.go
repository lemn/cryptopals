package crypto

import (
	"crypto/aes"
	"errors"
)

type ECBCipher struct {
	Size int
	Key  []byte
}

type CBCCipher struct {
	Size int
	Key  []byte
	Iv   []byte
}

func NewCBCCipher(size int, key []byte, iv []byte) (*CBCCipher, error) {
	if size != len(key) {
		return nil, errors.New("aes: CBC key length does not equal block size")
	}
	if size != len(iv) {
		return nil, errors.New("aes: CBC initialization vector does not equal block size")
	}
	return &CBCCipher{Size: size, Key: key, Iv: iv}, nil
}

func NewECBCipher(size int, key []byte) (*ECBCipher, error) {
	if size != len(key) {
		return nil, errors.New("aes: ECB key length does not equal block size")
	}
	return &ECBCipher{Size: size, Key: key}, nil
}

func (c ECBCipher) Encrypt(out []byte, in []byte) {
	cipher, _ := aes.NewCipher([]byte(c.Key))

	for bs, be := 0, c.Size; be <= len(in); bs, be = bs+c.Size, be+c.Size {
		cipher.Encrypt(out[bs:be], in[bs:be])
	}
}

func (c ECBCipher) Decrypt(out []byte, in []byte) {
	cipher, _ := aes.NewCipher([]byte(c.Key))

	for bs, be := 0, c.Size; be <= len(in); bs, be = bs+c.Size, be+c.Size {
		cipher.Decrypt(out[bs:be], in[bs:be])
	}
}

func (c CBCCipher) Encrypt(out []byte, in []byte) {
	cipher, _ := aes.NewCipher([]byte(c.Key))

	for bs, be := 0, c.Size; be <= len(in); bs, be = bs+c.Size, be+c.Size {
		if bs == 0 {
			cipher.Encrypt(out[bs:be], Xor(in[bs:be], c.Iv))
		} else {
			cipher.Encrypt(out[bs:be], Xor(in[bs:be], out[bs-c.Size:be-c.Size]))
		}
	}
}

func (c CBCCipher) Decrypt(out []byte, in []byte) {
	cipher, _ := aes.NewCipher([]byte(c.Key))
	dec := make([]byte, len(c.Key))

	for bs, be := 0, c.Size; be <= len(in); bs, be = bs+c.Size, be+c.Size {
		if bs == 0 {
			cipher.Decrypt(dec, in[bs:be])
			for i, v := range Xor(dec, c.Iv) {
				out[i] = v
			}
		} else {
			cipher.Decrypt(dec, in[bs:be])
			for i, v := range Xor(dec, in[bs-c.Size:be-c.Size]) {
				out[bs+i] = v
			}
		}
	}
}
